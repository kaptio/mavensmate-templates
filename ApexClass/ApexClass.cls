/*
Name:  {{ api_name }}.cls
Copyright © 2014  Kaptio ehf.
======================================================
======================================================
Purpose:
-------
Controller class for the VF page - MyPage.page
======================================================
======================================================
History
------- 
Ver. Author        Date        Detail
1.0  YourNameHere   {{ date }}  Class creation.

*/
public with sharing class {{ api_name }} {
	 //& public-scoped properties.   
    //& End public-scoped properties.
                 
    //& private-scoped variables.
    //& End private-scoped variables.
     
    //& page initialisation code.
	public {{ api_name }}() {
		
	}
	//& End page initialisation code.
	
	//& page actions.
    //& End page actions.
     
    //& data access helpers (class methods accessed from binding expressions).
    //& End data access helpers.
     
    //& controller code Helpers (class methods providing helper functions to data access helpers or actions).
    //& End controller code helpers.
     
    //& inner classes (wrapper classes typically, extending SObjects for convenience in the UI).
    //& End inner classes.
}